package infra

import (
	"gitlab.com/Titouan-Esc/api_common/handler"
	"gitlab.com/Titouan-Esc/api_common/logger"
	"gitlab.com/Titouan-Esc/api_common/router"
	"gitlab.com/escbook/backend/user/interface/controller"
)

func Dispatch(sql handler.SQLHandler, logServ, logSess *logger.Log) *router.Router {
	userRouter := router.NewRouter()

	userController := controller.NewUserController(sql, logSess)

	userRouter.AddCORS()

	userRouter.AddRoute(" Retrieve all users ", "GET", "/user/getAll", userController.ShowAll, logServ)
	userRouter.AddRoute(" Retrieve one user ", "GET", "/user/getOne", userController.ShowOne, logServ)
	userRouter.AddRoute(" Create a user ", "POST", "/user/create", userController.Insert, logServ)
	userRouter.AddRoute(" Update a user ", "PUT", "/user/update", userController.Modify, logServ)
	userRouter.AddRoute(" Delete a user ", "DELETE", "/user/delete", userController.Destroy, logServ)
	userRouter.AddRoute(" Login ", "POST", "/user/login", userController.Connect, logServ)

	return userRouter
}
