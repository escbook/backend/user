package main

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/Titouan-Esc/api_common/env"
	"gitlab.com/Titouan-Esc/api_common/handler"
	"gitlab.com/Titouan-Esc/api_common/logger"
	"gitlab.com/escbook/backend/user/infra"
)

func main() {
	asciiArt := `
  _____          ____              _                       
 | ____|___  ___| __ )  ___   ___ | | __                   
 |  _| / __|/ __|  _ \ / _ \ / _ \| |/ /                   
 | |___\__ \ (__| |_) | (_) | (_) |   <                    
 |_____|___/\___|____/ \___/_\___/|_|\_\        _          
 | | | |___  ___ _ __    / ___|  ___ _ ____   _(_) ___ ___ 
 | | | / __|/ _ \ '__|___\___ \ / _ \ '__\ \ / / |/ __/ _ \
 | |_| \__ \  __/ | |_____|__) |  __/ |   \ V /| | (_|  __/
  \___/|___/\___|_|      |____/ \___|_|    \_/ |_|\___\___|
`
	environnement := env.NewEnv()
	environnement.LoadEnv()

	logServ := logger.GetLoggers().Server
	logSess := logger.GetLoggers().Session

	sql, err := handler.NewSqlHandler()
	if err != nil {
		logServ.Error("[DB] ", err.Error())
		return
	}

	router := infra.Dispatch(sql, logServ, logSess)

	address := os.Getenv("API_SERVER_HOST") + ":" + os.Getenv("API_SERVER_PORT")
	//addressSwagger := os.Getenv("API_HOST_SWAG") + ":" + os.Getenv("API_SERVER_PORT")

	fmt.Println(asciiArt)
	fmt.Println("Server running on " + address)
	if err := http.ListenAndServe(address, router.Handle); err != nil {
		logServ.Error("[RUN] ", err.Error())
		return
	}
}
