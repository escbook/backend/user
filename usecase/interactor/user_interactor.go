package interactor

import (
	"errors"
	"fmt"
	"gitlab.com/Titouan-Esc/api_common/utils"

	"gitlab.com/Titouan-Esc/api_common/handler"
	"gitlab.com/escbook/backend/user/domain/model"
	repository2 "gitlab.com/escbook/backend/user/interface/repository"
	"gitlab.com/escbook/backend/user/usecase/repository"
	"gorm.io/gorm"
)

type UserInteractor struct {
	IUserRepository repository.IUserRepository
}

// ------------------------------ Constructor ------------------------------

func NewUserInteractor(database *gorm.DB) *UserInteractor {
	return &UserInteractor{
		IUserRepository: repository2.UserRepository{
			Sql: handler.SQLHandler{
				Db: database,
			},
		},
	}
}

// ------------------------------ CRUD ------------------------------

func (ui *UserInteractor) ShowAll() (model.Users, error) {
	users, err := ui.IUserRepository.GetAll()
	return users, err
}

func (ui *UserInteractor) ShowOne(uid string) (model.User, error) {
	user, err := ui.IUserRepository.GetOne(uid)
	return user, err
}

func (ui *UserInteractor) Insert(user model.User) (model.User, error) {
	exist := ui.IUserRepository.IsExistByEmail(user.Email)

	if exist {
		return model.User{}, errors.New(fmt.Sprintf("User [%s] already exist", user.Email))
	}

	user, err := ui.IUserRepository.Create(user)
	return user, err
}

func (ui *UserInteractor) Modify(target, patch model.User) error {
	exist := ui.IUserRepository.IsExist(target.Id)
	if !exist {
		return errors.New(fmt.Sprintf("User [%s] not found", target.Id))
	}

	if err := utils.Bind(&target, patch); err != nil {
		return err
	}

	err := ui.IUserRepository.Update(target)
	return err
}

func (ui *UserInteractor) Destroy(uid string) error {
	user, err := ui.IUserRepository.GetOne(uid)
	if err != nil {
		return err
	}

	exist := ui.IUserRepository.IsExist(user.Id)

	if !exist {
		return errors.New(fmt.Sprintf("User [%s] not found", user.Id))
	}

	newErr := ui.IUserRepository.Delete(uid)
	return newErr
}
