package test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Titouan-Esc/api_common/test"
	"gitlab.com/escbook/backend/user/domain/model"
	"gitlab.com/escbook/backend/user/usecase/interactor"
	"gorm.io/gorm"
)

type UserInteractorSetup struct {
	UI      *interactor.UserInteractor
	DB      *gorm.DB
	Suite   *test.Suite[model.User]
	NewUser model.User
	User    model.User
	UID     string
}

func NewUserInteractorSetup(t *testing.T) *UserInteractorSetup {
	suite, db, _, err := test.SetUp[model.User]("user_interactor_test.db")
	assert.Nil(t, err)

	ui := interactor.NewUserInteractor(db)

	errMigrate := db.AutoMigrate(model.User{})
	assert.Nil(t, errMigrate)

	return &UserInteractorSetup{
		UI:    ui,
		DB:    db,
		Suite: suite,
		User: model.User{
			UserName: "Test",
			Email:    "test@test.com",
			Password: "Test123",
		},
	}
}

func (s *UserInteractorSetup) TestUserInteractor_Insert(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Interactor Insert",
		Body: s.User,
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		user, err := s.UI.Insert(s.Suite.Data.Body)
		assert.Nil(t, err)

		s.UID = user.Id

		retour := s.DB.Table("users").Where("user_name = ?", s.Suite.Data.Body.UserName).First(&s.NewUser)
		assert.Nil(t, retour.Error)

		assert.Equal(t, s.NewUser.Id, user.Id)
		assert.Equal(t, s.NewUser.UserName, user.UserName)
	})
}
func (s *UserInteractorSetup) TestUserInteractor_ShowAll(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Interactor Show All",
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		users, err := s.UI.ShowAll()
		assert.Nil(t, err)

		assert.NotEmpty(t, users)
	})
}
func (s *UserInteractorSetup) TestUserInteractor_ShowOne(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Interactor Show One",
		Body: s.User,
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		user, err := s.UI.ShowOne(s.UID)
		assert.Nil(t, err)

		assert.NotEmpty(t, user)
		assert.Equal(t, s.Suite.Data.Body.Email, user.Email)
	})
}
func (s *UserInteractorSetup) TestUserInteractor_Modify(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Interactor Modify",
		Body: model.User{
			UserName: "NewTest",
			Email:    "newtest@newtest.com",
		},
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		retour := s.DB.Table("users").Where("id = ?", s.UID).First(&s.User)
		assert.Nil(t, retour.Error)

		err := s.UI.Modify(s.User, s.Suite.Data.Body)
		assert.Nil(t, err)

		result := s.DB.Table("users").Where("id = ?", s.User.Id).First(&s.NewUser)
		assert.Nil(t, result.Error)

		// IF is NOT Empty
		assert.NotEmpty(t, s.NewUser)

		// IS is Equal
		assert.Equal(t, s.User.Id, s.NewUser.Id)
		assert.Equal(t, s.User.Password, s.NewUser.Password)

		// IS is NOT Equal
		assert.NotEqual(t, s.User.UserName, s.NewUser.UserName)
		assert.NotEqual(t, s.User.Email, s.NewUser.Email)
	})
}
func (s *UserInteractorSetup) TestUserInteractor_Destroy(t *testing.T) {
	s.Suite.Data = test.Data[model.User]{
		Name: "User Interactor Destroy",
	}

	t.Run(s.Suite.Data.Name, func(t *testing.T) {
		err := s.UI.Destroy(s.UID)
		assert.Nil(t, err)

		retour := s.DB.Table("users").Where("id = ?", s.UID).First(&s.NewUser)
		assert.NotNil(t, retour.Error)
	})
}

func TestUserInteractor(t *testing.T) {
	s := NewUserInteractorSetup(t)

	s.TestUserInteractor_Insert(t)
	s.TestUserInteractor_ShowAll(t)
	s.TestUserInteractor_ShowOne(t)
	s.TestUserInteractor_Modify(t)
	s.TestUserInteractor_Destroy(t)

	defer func() {
		if err := os.Remove("user_interactor_test.db"); err != nil {
			t.Log(err.Error())
		}
	}()
}
