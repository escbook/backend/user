package repository

import "gitlab.com/escbook/backend/user/domain/model"

type IUserRepository interface {
	// ------------------------- Exist -------------------------

	IsExist(uid string) bool
	IsExistByEmail(email string) bool

	// ------------------------- CRUD --------------------------

	GetAll() (model.Users, error)
	GetOne(uid string) (model.User, error)
	Create(data model.User) (model.User, error)
	Update(data model.User) error
	Delete(uid string) error
}
