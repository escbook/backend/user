package controller

import (
	"gitlab.com/Titouan-Esc/api_common/controller"
	"gitlab.com/Titouan-Esc/api_common/handler"
	"gitlab.com/Titouan-Esc/api_common/logger"
	"gitlab.com/Titouan-Esc/api_common/utils"
	"gitlab.com/escbook/backend/user/domain/model"
	"gitlab.com/escbook/backend/user/interface/repository"
	"gitlab.com/escbook/backend/user/usecase/interactor"
	"net/http"
)

type UserController struct {
	UserInteractor interactor.UserInteractor
	Logger         *logger.Log
}

func NewUserController(sql handler.SQLHandler, logger *logger.Log) *UserController {
	return &UserController{
		UserInteractor: interactor.UserInteractor{
			IUserRepository: &repository.UserRepository{
				Sql:    sql,
				Logger: logger,
			},
		},
		Logger: logger,
	}
}

func (uc *UserController) ShowAll(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, uc.Logger)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	users, err := uc.UserInteractor.ShowAll()
	if err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	manager.Respons().Build(http.StatusOK, users)
}

func (uc *UserController) ShowOne(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, uc.Logger)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	user, err := uc.UserInteractor.ShowOne(manager.UserId)
	if err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	var newUser model.User
	newUser.Id = user.Id
	newUser.UserName = user.UserName
	newUser.Email = user.Email
	newUser.Image = user.Image

	manager.Respons().Build(http.StatusOK, newUser)
}

func (uc *UserController) Insert(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, uc.Logger, false)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	body := utils.ReadBody[model.User](manager.Body)

	user, err := uc.UserInteractor.Insert(body)
	if err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	mapUser := make(map[string]string)
	mapUser["name"] = user.UserName
	mapUser["email"] = user.Email
	mapUser["password"] = user.Password

	if err := manager.Cognito.SignUp(mapUser); err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	manager.Respons().Build(http.StatusCreated, user)
}

func (uc *UserController) Modify(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, uc.Logger)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	user, err := uc.UserInteractor.ShowOne(manager.UserId)
	if err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	body := utils.ReadBody[model.User](manager.Body)

	if err := uc.UserInteractor.Modify(user, body); err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	manager.Respons().Build(http.StatusOK, nil)
}

func (uc *UserController) Destroy(res http.ResponseWriter, req *http.Request) {
	manager := controller.NewController(res, req, uc.Logger)
	if manager.Errors.Error {
		manager.StopRequest()
		return
	}

	if err := uc.UserInteractor.Destroy(manager.UserId); err != nil {
		manager.Respons().Build(http.StatusInternalServerError, err.Error())
		return
	}

	manager.Respons().Build(http.StatusOK, nil)
}

func (uc *UserController) Connect(res http.ResponseWriter, req *http.Request) {
	// implement me
	panic("implement me")
}
