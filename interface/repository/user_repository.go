package repository

import (
	"github.com/google/uuid"
	"gitlab.com/Titouan-Esc/api_common/handler"
	"gitlab.com/Titouan-Esc/api_common/logger"
	"gitlab.com/Titouan-Esc/api_common/middlewares"
	"gitlab.com/escbook/backend/user/domain/model"
)

type UserRepository struct {
	Sql    handler.SQLHandler
	Logger *logger.Log
}

func (u UserRepository) IsExist(uid string) bool {
	var user model.User

	retour := u.Sql.Db.Table("users").Where("id = ?", uid).First(&user)
	if retour.Error != nil {
		return false
	}

	if user.Id == "" {
		u.Logger.Error("User not found in database")
		return false
	} else {
		return true
	}
}

func (u UserRepository) IsExistByEmail(email string) bool {
	var user model.User

	retour := u.Sql.Db.Table("users").Where("email = ?", email).First(&user)
	if retour.Error != nil {
		return false
	}

	if user.Id == "" {
		u.Logger.Error("User not found in database")
		return false
	} else {
		return true
	}
}

func (u UserRepository) GetAll() (model.Users, error) {
	var users model.Users

	if retour := u.Sql.Db.Table("users").Find(&users); retour.Error != nil {
		u.Logger.Error(retour.Error)
		return model.Users{}, retour.Error
	}

	return users, nil
}

func (u UserRepository) GetOne(uid string) (model.User, error) {
	var user model.User

	if retour := u.Sql.Db.Table("users").Where("id = ?", uid).Select("id, email, user_name, image").First(&user); retour.Error != nil {
		u.Logger.Error(retour.Error)
		return model.User{}, retour.Error
	}

	return user, nil
}

func (u UserRepository) Create(data model.User) (model.User, error) {
	var user model.User

	data.Id = uuid.New().String()
	data.Password = middlewares.HashPassword(data.Password)

	if retour := u.Sql.Db.Table("users").Create(&data); retour.Error != nil {
		u.Logger.Error(retour.Error)
		return model.User{}, retour.Error
	}

	user.Id = data.Id
	user.UserName = data.UserName
	user.Email = data.Email
	user.Image = data.Image

	return user, nil
}

func (u UserRepository) Update(data model.User) error {
	if retour := u.Sql.Db.Table("users").Save(&data); retour.Error != nil {
		u.Logger.Error(retour.Error)
		return retour.Error
	}

	return nil
}

func (u UserRepository) Delete(uid string) error {
	var user model.User

	if retour := u.Sql.Db.Table("users").Where("id = ?", uid).Delete(&user); retour.Error != nil {
		u.Logger.Error(retour.Error)
		return retour.Error
	}

	return nil

}
