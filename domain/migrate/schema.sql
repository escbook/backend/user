CREATE DATABASE "eb_user";

CREATE SCHEMA IF NOT EXISTS "develop";

CREATE TABLE IF NOT EXISTS "develop"."users" (
    "id" varchar PRIMARY KEY,
    "user_name" varchar UNIQUE NOT NULL,
    "email" varchar UNIQUE NOT NULL,
    "password" varchar NOT NULL,
    "image" varchar,
    "date_created" timestamp DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT valid_email CHECK (position('@' in email) > 0)
);