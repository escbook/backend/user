package model

type User struct {
	Id       string `json:"id"`
	UserName string `json:"user_name"`
	Age      string `json:"age"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Image    string `json:"image"`
}

type Users []User
